# MobAd SDK - 2.1.2
- [Getting Started](#getting-started)
  - [Prerequisites:](#prerequisites)
  - [Installation](#installation)
    - [Swift Package Manager](#swift-package-manager)
- [Usage](#usage)
  - [MobAd Configurations](#mobad-configurations)
    - [Request Permission for ATTracking](#request-permission-for-attracking)
    - [Create a new Property List file:](#create-a-new-property-list-file)
    - [Import the module to your project:](#import-the-module-to-your-project)
    - [AppDelegate Configuration](#appdelegate-configuration)
  - [Fetch full screen ad](#fetch-full-screen-ad)
    - [SwiftUI](#swiftui)
    - [UIKit](#uikit)
  - [Get notification ad](#get-notification-ad)
    - [Local Notification](#local-notification)
    - [Remote Notification](#remote-notification)
  - [Display Banner Ad](#display-banner-ad)
    - [SwiftUI](#swiftui-1)
    - [UIKit](#uikit-1)
  - [Native Ads](#native-ads)
    - [SwiftUI](#swiftui-2)
  - [Update User Profile](#update-user-profile)
    - [Fetch Languages](#fetch-languages)
  - [Add/Remove interests](#addremove-interests)
    - [Add Interest](#add-interest)
    - [Remove Interest](#remove-interest)
  - [Fetch category](#fetch-category)
  - [Fetch User Profile](#fetch-user-profile)
- [MobAd.plist](#mobadplist)

# Getting Started
The following instructions will allow you to integrate MobAd SDK to your iOS application using XCode.

## Prerequisites:
- Install the following:
   - Xcode 14.1 or later
- Make sure that your project meets these requirements:
  - Your project must target these platform versions or later:
    - iOS 15.0 or later
    - iPadOS 15.0 or later

## Installation
### Swift Package Manager

Use [Swift Package Manager](https://developer.apple.com/documentation/xcode/adding-package-dependencies-to-your-app) to install and manage MobAd.


In Xcode, with your app project open, navigate to `File` > `Add Packages`.
When prompted, add `MobAd` Apple platforms SDK repositories:
```
https://gitlab.com/imw-developers/MobAdSDK-iOS.git
```
> We recommend using the default (latest) SDK version.
# Usage

## MobAd Configurations
### Request Permission for ATTracking

In the `Info.plist` file, add the following property:

```xml
<key>NSUserTrackingUsageDescription</key>
<string>This app uses your data to provide personalized advertisements.</string>
```
>  **Warning:** If the property is not added, the app will crash.
>

In case you are using `SwiftUI`, to access the `Info.plist` file, go to Targets > Info, then add the required field: `Privacy - Tracking Usage Description`.

### Create a new Property List file:

Name your file `MobAd.plist` and paste in the code (code will be provided, but here is a [template](#mobadplist)).

> **Warning:** do not track `MobAd.plist` with git.
>
> Make sure to add `MobAd.plist` to `.gitignore` file.


### Import the module to your project:

```swift
import MobAdSDK
```
> always import the module when you need to use MobAd
>
> After importing, if the module is not found, try building the project or clean build. In case of failure to get rid of the error message, go to `Target` > `Build Phases` and check if MobAdSDK is in `Embed Frameworks` or `Link Binary With Libraries`.

### AppDelegate Configuration
In your AppDelegate class, inside the `didFinishLaunchingWithOptions` function, call the `configure(for:)` function like so:

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    
    // **IMPORTANT:** Actions, such as clicks and impressions, only counts in production
    MobAd.shared.configure(for: <#Environment#>)

  return true
}
```

And in case you are using `SwiftUI` it is the same process:
```swift
import SwiftUI
import MobAdSDK

class AppDelegate: NSObject, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        // **IMPORTANT:** Actions, such as clicks and impressions, only counts in production
        MobAd.shared.configure(for: <#Environment#>)
        return true
    }
}

@main
struct MyApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

```

> For `Environment`, Use `.developement` for development and testing, and `.production` for releases and updates.
>
> **IMPORTANT:** Actions, such as clicks and impressions, only counts in production

And now, you can create a shared instance of MobAd with:

```swift
let mobad = MobAd.shared
```

> Please note that in order to fetch an ad or call any other function, `configure(for:)` must be called.

## Fetch full screen ad

### SwiftUI
Fetch the ad with:

```swift
// error: Error
MobAd.shared.getAd(type: <#ContentType#>) { error in
    if let error = error {
        // handle errors
    }
}
```

### UIKit
For UIKit, same process:

```swift
MobAd.shared.getAd(type: <#ContentType#>) { error in
    if let error = error {
        // handle errors
    }
}
```

## Get notification ad 
### Local Notification
In the Notification Delegate Class, add the following code to the `didReceive:` userNotification function:
```swift
mobad.handleMobAdNotifications(of: response)
```

The function should look like this:

```swift
func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    mobad.handleMobAdNotifications(of: response)
    completionHandler()
}
```

To schedule the notification, you can use the following lines of code:

```swift
MobAd.shared.getAd(type: <#ContentType#>, delayInMinutes: <#Double#>) { error in
    if let error = error {
        // handle error
    }
}
```
> Delay time should be specified in minutes
> Please note that the maximum delay is 60 minutes

In case you do not have a notification delegate class, please do the following:
- Create a new file `NotificationHandlerDelegate`.
- Paste this code in it: 
```swift
import Foundation
import UserNotifications
import MobAdSDK

class NotificationHandlerDelegate: NSObject, UNUserNotificationCenterDelegate{
    
    private let mobad = MobAd.shared
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        mobad.handleMobAdNotifications(of: response)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .list, .sound])
    }
}
```
- And finally, in the AppDelegate:
```swift
 let notificationHandlerDelegate = NotificationHandlerDelegate()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        ...
        
        UNUserNotificationCenter.current().delegate = notificationHandlerDelegate
        return true
    }
```

### Remote Notification
For remote notifications, please import [OneSignal's framework](https://documentation.onesignal.com/docs/ios-sdk-setup), and prepare the same setup of local notifications.

Be sure to enable `Background Fetch`, `Background Processing` and `Remote Notification` in the Target's `Signing & Capabilities`.

> The integration of OneSignal is temporary

First of all in `didFinishLaunchingWithOptions`, paste the following:

```swift
// import OneSignal
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

      OneSignal.initialize("APP_ID", withLaunchOptions: launchOptions)
    
      MobAd.shared.configure(for: .development) { userId, error, _ in
          if let error = error {
              //handle error
          } else {
              guard let userId = userId else {
                //handle error
                return
              }

              OneSignal.login(userId)
          }
      }
    
    return true
}
```
Next, Go to `File` > `New` > `Target` and create a new `Notification Service Extension`, name it `OneSignalNotificationServiceExtension`

Inside the newly created folder, delete the content of the `NotificationService` class and paste in the follwing:

```swift
import UserNotifications
import OneSignalExtension

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var receivedRequest: UNNotificationRequest!
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.receivedRequest = request
        self.contentHandler = contentHandler
        self.bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            /* DEBUGGING: Uncomment the 2 lines below to check this extension is executing
             Note, this extension only runs when mutable-content is set
             Setting an attachment or action buttons automatically adds this */
            // print("Running NotificationServiceExtension")
            // bestAttemptContent.body = "[Modified] " + bestAttemptContent.body
            
            OneSignalExtension.didReceiveNotificationExtensionRequest(self.receivedRequest, with: bestAttemptContent, withContentHandler: self.contentHandler)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            OneSignalExtension.serviceExtensionTimeWillExpireRequest(self.receivedRequest, with: self.bestAttemptContent)
            contentHandler(bestAttemptContent)
        }
    }
}
```
The user should be able to receive notifications after that

## Display Banner Ad
To display a banner on your app screen, it can be done with a few lines.

### SwiftUI
It very simple to add a banner in SwiftUI, just paste the following line:

```swift
.addBanner(at: <#Position#>, type: <#BannerType#>)
```
This modifier can be added like so:

```swift
// DO NOT DO THIS UNLESS THE ELEMNTS TAKES UP THE ENTIRE SCREEN
struct ContentView: View {
   
    var body: some View {

        VStack {
            Image(systemName: "globe")
            Text("Hello World!")
        }
        .addBanner(at: <#Position#>, type: <#BannerType#>)
    }
}
```
Or
```swift
// but this can cause problems when using other elements, not limited to NavigationView or NavigationStack
ContentView()
  .addBanner(at: <#Position#>, type: <#BannerType#>)
```

### UIKit
If you are using UIKit, please add the following in the `viewDidLoad` function 

```swift
mobad.displayBannerAd(toParent: self, position: <#Position#>, type: <#BannerType#>)
```

> Be aware that the banner will display **ON TOP** of the view, so some UI element may be hidden after adding the banner, Fix your View/ViewController accordingly.

## Native Ads
If you want to add Native, customizable ads for so that it blends with your app, do the following:

In the `AppDelegate` add the following:
```swift
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
      
     ...

      MobAd.shared.configure(for: .development) { _, _, nativeAdsReady in
      ...
      // notification setup (if any)
      ...
          
          if nativeAdsReady {
              MobAdLoader.shared.setupNativeAds(with: <#[NativeAdType]#>)
          }
      }
    ...
    return true
}
```
> `[NativeAdType]` is an array that can have `.image`, `.video` or both 

### SwiftUI

```swift
import SwiftUI
import MobAdSDK

struct NewAd: View {
    
    let nativeAd: NativeAd
    
    var body: some View {
        
        MobAdNativeAdView(nativeAd: nativeAd, adIconPosition: .topRight) {
            VStack{
                 // MobAdMedia can be either an image or a video, you can choose
                MobAdMedia(media: nativeAd.media, isClickable: true)
                    .frame(height: 100)
                    
                Text(nativeAd.title)
                    .font(.headline)
                    .lineLimit(1)
                Text(nativeAd.description)
                    .font(.body)
                    .lineLimit(3)

                // MobAdButton is the CTA, you can use it to let the user go to the webpage
                MobAdButton(button: nativeAd.button, labels: ButtonLabel(nativeAd: nativeAd))
                
            }
        }
    }
}

struct ButtonLabel: View {

    let nativeAd: NativeAd

    var body: some View {
        Text(nativeAd.button.title)
    }
}
```
> You can come up with a design suitable for your app.

## Update User Profile
To update the user's profile, you can simply use the following code: 

```swift
mobad.updateProfile(adCap: <#Int#>, isOptedOut: <#Bool#>, languageIdList: <#[String]#>)
```

Or if you want to use a completion handler :
```swift
mobad.updateProfile(adCap: <#Int#>, isOptedOut: <#Bool#>, languageIdList: <#[String]#>) { responseMessage in
    // handle completion
}
```

In `updateProfile()`, you can set:
- `adCap`: which is the maximum numbers of ad the user can receive per day (50 max).
- `isOptedOut`: if true, the user will not receive ads when `getAd` is called.
- `languageIdList`: is an array of strings that accepts Language IDs (i.e: `en` for English, `fr` for French...).

### Fetch Languages

You can let the user select their languages with the following function:

```swift
mobad.fetchLanguages { languages, error in
    if let error = error {
        // handle error
    } else {
        guard let languages = languages else {
            return
        }
        // do something
    }
}
```
> You can use a TableView or a List to list these languages for your users and then `updateProfile()` once the lanagues are selected. Or you can by default set it to a default array of languages.


## Add/Remove interests
For a personalized experience for your users, you can let the user receive an ad that may interest them by adding or removing topics, check out the interests [here](#fetch-category).

### Add Interest
```swift
mobad.addInterest(interest: <#[String]#>)
```

### Remove Interest
```swift
mobad.removeInterest(interest: <#[String]#>)
```

Both of them have completion handlers too:

```swift
mobad.addInterest(interest: <#[String]#>){ responseMessage in
    // handle completion
}


mobad.removeInterest(interest: <#[String]#>){ responseMessage in
    // handle completion
}
```

## Fetch category
You can get all the available categories by running the following:
```swift
mobad.fetchCategories { categories, error in
    if let error = error {
        // handle error
    } else {
        // handle the response.
    }
}
```
> you can create a List or a TableView of categories, and let the users select their interests.
>
> This can be useful if you want to create a form for your users and let them customize their own ad experience.

## Fetch User Profile
You can fetch the user's profile with the following:
```swift
mobad.fetchProfile { user, error in
    if let error = error {
        // handle error
    } else {
        // handle user's data.
    }
}
```

# MobAd.plist
```plist
<key>appId</key>
<string>YOUR_APP_ID</string>

<key>machineSecret</key>
<string>YOUR_MACHINE_SECRET</string>

<key>notificationAppId</key>
<string>YOUR_NOTIFICATION_APP_ID</string>
```
> Do not forget to input the application's ID, the machine secret and the Notification App Id.


In case of any inconvenience, you're welcome to contact our support team by emailing [support@i-magineworks.com](mailto:support@i-magineworks.com)
