// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9.2 (swiftlang-5.9.2.2.56 clang-1500.1.0.2.5)
// swift-module-flags: -target arm64-apple-ios15.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MobAdSDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AdSupport
import AppTrackingTransparency
import DeveloperToolsSupport
import Foundation
@_exported import MobAdSDK
import Swift
import SwiftUI
import UIKit
import UserNotifications
import WebKit
import _AVKit_SwiftUI
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public enum NativeAdType : Swift.String {
  case image
  case video
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct NativeAd {
  public let title: Swift.String
  public let description: Swift.String
  public let iconUrl: Swift.String
  public let advertiserName: Swift.String
  public let button: MobAdSDK.NativeButton
  public let media: MobAdSDK.NativeMedia
}
public enum ContentType : Swift.String {
  case carousel, text, image, video, fullImage, interactive
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct MobAdMedia : SwiftUI.View {
  public init(media: MobAdSDK.NativeMedia, isClickable: Swift.Bool)
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s8MobAdSDK0aB5MediaV4bodyQrvp", 0) __
}
public enum BannerType : Swift.String, Swift.CaseIterable {
  case image
  case video
  public init?(rawValue: Swift.String)
  public typealias AllCases = [MobAdSDK.BannerType]
  public typealias RawValue = Swift.String
  public static var allCases: [MobAdSDK.BannerType] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public enum MobAdError : Foundation.LocalizedError {
  case propertyListNotFound
  case appIdNotFound
  case apiKeyNotFound
  case tokenNotFound
  case userNotFound
  case failedInitialization
  case failedToUpdateUser
  case failedToFetchProfile
  case failedToGetLanguages
  case failedToLoginMachine
  case categoriesNotFound
  case requestFailure
  case notificationPermissionFailure
  case notificationScheduled
  case MobAdLoaderType
  case failedToRegisterDevice
  case deviceAlreadyRegistered
  case adIdNotFound
  case actionTokensNotFound
  case notificationFailure
  case imageFailure
  case unknown(errorMessage: Swift.String)
  public var errorDescription: Swift.String {
    get
  }
}
public struct Categories : Swift.Codable {
  public var id: Swift.String?
  public var name: Swift.String?
  public var subCategories: [MobAdSDK.Subcategories]?
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
public enum BannerPosition {
  case top
  case bottom
  public static func == (a: MobAdSDK.BannerPosition, b: MobAdSDK.BannerPosition) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum ADIconPosition {
  case topRight
  case topLeft
  case bottomLeft
  case bottomRight
  public static func == (a: MobAdSDK.ADIconPosition, b: MobAdSDK.ADIconPosition) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct NativeButton {
  public let title: Swift.String
}
public enum Environments {
  case production
  case development
  public static func == (a: MobAdSDK.Environments, b: MobAdSDK.Environments) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct MobAdButton<Content> : SwiftUI.View where Content : SwiftUI.View {
  public init(button: MobAdSDK.NativeButton, @SwiftUI.ViewBuilder label: @escaping () -> Content)
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s8MobAdSDK0aB6ButtonV4bodyQrvp", 0) __<Content>
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) public class BannerAdViewController : UIKit.UIViewController {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
public struct Language : Swift.Codable, Swift.Hashable {
  public var id: Swift.String?
  public var name: Swift.String?
  public init(from decoder: any Swift.Decoder) throws
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: MobAdSDK.Language, b: MobAdSDK.Language) -> Swift.Bool
  public func encode(to encoder: any Swift.Encoder) throws
  public var hashValue: Swift.Int {
    get
  }
}
public struct MobAdNativeAdView<Content> : SwiftUI.View where Content : SwiftUI.View {
  public init(nativeAd: MobAdSDK.NativeAd, adIconPosition: MobAdSDK.ADIconPosition, @SwiftUI.ViewBuilder content: () -> Content)
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s8MobAdSDK0ab6NativeB4ViewV4bodyQrvp", 0) __<Content>
}
public struct NativeMedia {
  public let mediaUrl: Swift.String
}
@_hasMissingDesignatedInitializers public class MobAdLoader {
  public static let shared: MobAdSDK.MobAdLoader
  public var nativeImageAd: MobAdSDK.NativeAd? {
    get
  }
  public var nativeVideoAd: MobAdSDK.NativeAd? {
    get
  }
  public func setupNativeAds(with types: [MobAdSDK.NativeAdType], completion: ((Swift.String) -> Swift.Void)? = nil)
  @objc deinit
}
public struct Subcategories : Swift.Codable {
  public var id: Swift.String?
  public var name: Swift.String?
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
extension SwiftUI.View {
  public func addBanner(at position: MobAdSDK.BannerPosition, type: MobAdSDK.BannerType) -> some SwiftUI.View
  
}
public struct User : Swift.Codable {
  public var id: Swift.String?
  public var countryId: Swift.String?
  public var isOptedOut: Swift.Bool?
  public var adCap: Swift.Int?
  public var languageIdList: [Swift.String]?
  public var interestIds: [Swift.String]?
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
@_hasMissingDesignatedInitializers public class MobAd {
  public static let shared: MobAdSDK.MobAd
  public var notificationId: Swift.String? {
    get
  }
  public func configure(for environment: MobAdSDK.Environments, completion: ((Swift.String?, MobAdSDK.MobAdError?, Swift.Bool) -> Swift.Void)? = nil)
  public func getAd(type: MobAdSDK.ContentType, completion: ((MobAdSDK.MobAdError?) -> Swift.Void)? = nil)
  public func getAd(type: MobAdSDK.ContentType, delayInMinutes: Foundation.TimeInterval, completion: @escaping (MobAdSDK.MobAdError?) -> Swift.Void)
  public func displayBannerAd(toParent viewController: UIKit.UIViewController, position: MobAdSDK.BannerPosition, type: MobAdSDK.BannerType)
  public func updateProfile(adCap: Swift.Int?, isOptedOut: Swift.Bool?, languageIdList: [Swift.String]?, completion: ((Swift.String) -> Swift.Void)? = nil)
  public func addInterest(interest: [Swift.String], completion: ((Swift.String) -> Swift.Void)? = nil)
  public func removeInterest(interest: [Swift.String], completion: ((Swift.String) -> Swift.Void)? = nil)
  public func fetchProfile(completion: @escaping (MobAdSDK.User?, MobAdSDK.MobAdError?) -> Swift.Void)
  public func fetchLanguages(completion: @escaping ([MobAdSDK.Language]?, MobAdSDK.MobAdError?) -> Swift.Void)
  public func fetchCategories(completion: @escaping ([MobAdSDK.Categories]?, MobAdSDK.MobAdError?) -> Swift.Void)
  public func handleMobAdNotifications(of response: UserNotifications.UNNotificationResponse)
  @objc deinit
}
extension MobAdSDK.NativeAdType : Swift.Equatable {}
extension MobAdSDK.NativeAdType : Swift.Hashable {}
extension MobAdSDK.NativeAdType : Swift.RawRepresentable {}
extension MobAdSDK.ContentType : Swift.Equatable {}
extension MobAdSDK.ContentType : Swift.Hashable {}
extension MobAdSDK.ContentType : Swift.RawRepresentable {}
extension MobAdSDK.BannerType : Swift.Equatable {}
extension MobAdSDK.BannerType : Swift.Hashable {}
extension MobAdSDK.BannerType : Swift.RawRepresentable {}
extension MobAdSDK.BannerPosition : Swift.Equatable {}
extension MobAdSDK.BannerPosition : Swift.Hashable {}
extension MobAdSDK.ADIconPosition : Swift.Equatable {}
extension MobAdSDK.ADIconPosition : Swift.Hashable {}
extension MobAdSDK.Environments : Swift.Equatable {}
extension MobAdSDK.Environments : Swift.Hashable {}
