import XCTest
@testable import MobAdSDK_iOS

final class MobAdSDK_iOSTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MobAdSDK_iOS().text, "Hello, World!")
    }
}
